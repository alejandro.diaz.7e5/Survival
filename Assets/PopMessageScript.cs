using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopMessageScript : MonoBehaviour
{
    public void CloseMessage()
    {
        Debug.Log("Closing Message");
        LevelUISceneCalls.ClearMessage();
    }
}
