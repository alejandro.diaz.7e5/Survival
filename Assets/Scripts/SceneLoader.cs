﻿
using UnityEngine;
using UnityEngine.SceneManagement;
[CreateAssetMenu (fileName = "SceneLoader Unit", menuName = "Static/SceneLoader")]
public class SceneLoader : ScriptableObject
{
    public static void LoadLevelScene(string level)
    {
        SceneManager.LoadScene(level,LoadSceneMode.Single);
        SceneManager.LoadScene("LevelUI", LoadSceneMode.Additive);
    }
    public static void LoadGameOverScene(string message)
    {
        SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
        GameObject.Find("Message").GetComponent<TMPro.TMP_Text>().text = message;
    }
}
