using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Item",menuName ="Scriptables/Item")]
public class Item : ScriptableObject
{
    public GameObject itemObject;
    public Sprite displayImage;
    public float Value;
    public string description;
    public int MaxAmount;
    public bool isAttachable;
    public GameObject Attachable;
}
