using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public Inventory inventory;
    InvSlotManager[] slots;
    // Start is called before the first frame update
    void Start()
    {
        slots = GetComponentsInChildren<InvSlotManager>();
        LoadItems();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadItems()
    {
        for (int i = 0; i < inventory.items.Count; i++)
        {
            slots[i].UpdateSlot(inventory.items[i].displayImage, inventory.itemCount[i]);
        }

    }
}
