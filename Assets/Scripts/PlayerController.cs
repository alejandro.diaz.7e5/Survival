
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public PlayerInput input;
    public Inventory playerInventory;
    public GameObject inventoryPanel;
    public GameObject gun;
    public CharacterController playerCC;
    public Animator animator;
    public CinemachineVirtualCamera playerCamera;
    public CinemachineVirtualCamera aimCamera;
    public CinemachineVirtualCamera victoryCamera;

    public Collider reach;
    private Quaternion q;
    private float speed = 1;
    private float jump = -5;
    bool crouch = false;
    bool run = false;
    bool aiming = false;
    float smoothSpeedX = 0f;
    float smoothSpeedY = 0f;
    Vector2 movement;
    Vector2 currentMovementAnim;
    Coroutine jumpCrt;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        victoryCamera.enabled = false;
        LevelUISceneCalls.InitScript();
        //StartCoroutine(IdleTimer());

    }

    // Update is called once per frame
    void Update()
    {
        //var direction = new Vector3(movement.x, 0, movement.y);
        var direction = transform.forward* movement.y + transform.right * movement.x;
        direction =  q * direction;
        direction.y += jump/speed;
        playerCC.Move(direction * Time.deltaTime * speed);
        currentMovementAnim.x = Mathf.SmoothDamp(currentMovementAnim.x, movement.x, ref smoothSpeedX, 0.5f);
        currentMovementAnim.y = Mathf.SmoothDamp(currentMovementAnim.y, movement.y, ref smoothSpeedY, 0.5f);
        animator.SetFloat("Side Input", currentMovementAnim.x);
        animator.SetFloat("Forward Input", currentMovementAnim.y);
    }

    public void MovePlayer(InputAction.CallbackContext context)
    {
        movement = context.ReadValue<Vector2>();
       
    }
    public void RotatePlayer(InputAction.CallbackContext context)
    {
        transform.Rotate(new Vector3(0, context.ReadValue<Vector2>().x, 0) * 0.1f);
        q = Quaternion.Euler(new Vector3(0, context.ReadValue<Vector2>().x, 0));
    }
    public void ToggleCrouch(InputAction.CallbackContext context)
    {
        crouch = !crouch;
        speed = crouch ? 0.5f : 1;
        animator.SetBool("Crouch", crouch);
        if (crouch)
            input.actions.FindAction("Fire").Disable();
        else
            input.actions.FindAction("Fire").Enable();
    }
    public void ToggleRun(InputAction.CallbackContext context)
    {
        run = !run;
        speed = run ? 4 : 1;
        animator.SetBool("Run", run);
    }
    public void Aim(InputAction.CallbackContext context)
    {
        aiming = !aiming;
        GetComponent<IKController>().ikActive = !GetComponent<IKController>().ikActive;
        animator.SetBool("Aim", aiming);
        aimCamera.enabled = aiming;
        

    }
    public void Shoot(InputAction.CallbackContext context)
    {
        if (context.performed) {
            RaycastHit hit;
            if (Physics.Raycast(gun.transform.position, gun.transform.right, out hit))
            {
                Debug.DrawRay(gun.transform.position, gun.transform.right * 10, Color.green, 1f);
                Debug.Log("Did Hit");
                if (hit.collider.TryGetComponent<ITargetable>(out ITargetable target)) target.Hit();
            }
            else
            {
                Debug.DrawRay(gun.transform.position, gun.transform.right * 10, Color.red, 1f);
                Debug.Log("Did not Hit");
            } 
        }
        
    }
    public void Jump(InputAction.CallbackContext context)
    {
       
        if (context.started && playerCC.isGrounded)
        {
            animator.SetTrigger("Jump");
            jumpCrt = StartCoroutine(Jump());
        }
        if (context.canceled)
        {
            StopCoroutine(jumpCrt);
           jump = -5;
        }
           
    }

    private IEnumerator Jump()
    {
        
        int temp = 1;
        jump = 0f;
        while (temp < 5)
        {
            jump = 5;
            yield return new WaitForSeconds(0.1f);
            temp += 1;
        }
        jump = -5f;
        yield return null;
    }

    public void PickUp(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            animator.SetTrigger("Pickup");
            RaycastHit hit;
            if (Physics.BoxCast(transform.position - transform.forward, new Vector3(1, 1, 1), transform.forward, out hit, Quaternion.identity, 2f))
            { 
                Debug.Log(hit.collider.gameObject.tag + " hit");
                if (hit.collider.tag == "Pickable")
                    hit.collider.gameObject.GetComponent<PickableItem>().PickUp(playerInventory);
            }
        }
    }
    public void StartCelebration(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            input.DeactivateInput();
            animator.SetTrigger("Celeb");
            victoryCamera.enabled = true;
            StartCoroutine(WaitCelebration());
        }
    }
    public void StartTogglePanel(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            LevelUISceneCalls.OpenInventoryUI();
        }
    }

    public void EndCelebration()
    {
        input.ActivateInput();
        victoryCamera.enabled = false;
    }
    IEnumerator WaitCelebration()
    {
   
            yield return new WaitForSeconds(7f);
            EndCelebration();
    }
    IEnumerator IdleTimer()
    {
       // while (true)
       //{
            float timer = 0;
            while (!Input.anyKeyDown)
            {
                timer += Time.deltaTime;
                if (timer > 10)
                    animator.SetBool("DeepIdle", true);
            }
            timer = 0;
            animator.SetBool("DeepIdle", false);
         yield return null;
      // }

    }

}
