using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : PickableItem
{
    public GameObject message;
    public GameObject OpenChest;

    public override void PickUp(Inventory inventory)
    {
        if (inventory.items.Exists(x => x == item))
            SceneLoader.LoadGameOverScene("YOU Win!!!");
        else
            LevelUISceneCalls.ShowMessage(message);
    }
}
 