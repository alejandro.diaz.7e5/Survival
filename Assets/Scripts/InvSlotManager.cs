using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvSlotManager : MonoBehaviour
{
    public TMPro.TMP_Text quantity;
    public Image display;
    public void UpdateSlot(Sprite image, int amount)
    {
        display.sprite = image;
        quantity.text = amount.ToString();
    }
}
