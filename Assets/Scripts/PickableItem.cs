using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(requiredComponent:(typeof(AudioSource)))]
public class PickableItem : MonoBehaviour
{
    public ParticleSystem particles;
    public Item item;
    Inventory inventory;
    public enum PickType
    {
        Activable,
        Grabbable,
    }
    public PickType type;
    // Start is called before the first frame update
    void Start()
    {
        //inventory
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public virtual void PickUp(Inventory inventory)
    {
        bool itemFound = false;
        for (int i = 0; i < inventory.items.Count; i++)
        {
            if (inventory.items[i] == item)
            {
                inventory.itemCount[i]++;
                itemFound = true;
                break;
            }
        }
        if (itemFound == false)
        {
            inventory.items.Add(item);
            inventory.itemCount[inventory.items.Count-1] = 1;
            if (item.isAttachable)
                GameObject.Instantiate(item.Attachable, GameObject.Find("BackAttachment").transform);
        }
        GetComponent<AudioSource>().Play();
        switch (type)
        {
            case PickType.Activable:
                Activate();
                break;
            case PickType.Grabbable:
                StartCoroutine(Grab());
                break;
        }
    }
    void Activate()
    {
        particles.Stop();
        particles.Clear();
        this.gameObject.tag = "Untagged";
    }
    IEnumerator Grab()
    {
        this.gameObject.tag = "Untagged";
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }
}
