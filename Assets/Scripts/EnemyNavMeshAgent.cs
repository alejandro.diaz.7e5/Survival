using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyNavMeshAgent : MonoBehaviour
{
    public Transform goal;
    public Transform[] WalkPoints;
    public int[] route;
    public int routeIndex;
    public NavMeshAgent agent;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.destination = WalkPoints[route[routeIndex]].position;
    }
    // Update is called once per frame
    void Update()
    {
        if (agent.remainingDistance < 0.5f)
        {
            routeIndex++;
            if(routeIndex >= route.Length) routeIndex = 0;
            if (route[routeIndex] == -1) 
            { 
                StartCoroutine(Stop());
                routeIndex++;
            }
            agent.SetDestination(WalkPoints[route[routeIndex]].position);
        }
    }

    private IEnumerator Stop()
    {
        agent.isStopped = true;
        yield return new WaitForSeconds(5);
        agent.isStopped = false;
        yield return null;
    }
}
