using System;
using Unity.Collections;
using UnityEngine;
[CreateAssetMenu(fileName = "new BTNode", menuName = "Scriptables/BTNode")]
public class BTNode : ScriptableObject

{
    [SerializeReference]
    public ConditionSO condition;
    [SerializeReference]
    public ActionSO action;
    public BTNode parentNode;
    [ReadOnly]
    public BTNode[] childNodes;
    private void Awake()
    {
        ScanChildNodes();
    }
    // Start is called before the first frame update
    public void ScanChildNodes()
    {
        childNodes =  Array.FindAll(Resources.FindObjectsOfTypeAll<BTNode>(), node => node.parentNode == this);
        foreach (BTNode node in childNodes)
            node.ScanChildNodes();
    }
}
