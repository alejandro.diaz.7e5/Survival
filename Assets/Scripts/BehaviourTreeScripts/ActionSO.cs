using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionSO: ScriptableObject
{
    public abstract void OnSet(GameObject sceneObject);
    public abstract void OnUpdate();
    public abstract void OnExit(BTNodeManager manager);

}
