using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTNodeManager : MonoBehaviour
{
    public BTNode rootNode;
    public BTNode currentNode;
    public bool requireNewState = true;
    public bool validStateFound = false;
    // Start is called before the first frame update
    void Start()
    {
        currentNode = rootNode;
        rootNode.ScanChildNodes();
    }

    // Update is called once per frame
    void Update()
    {
        if (requireNewState)  
            requireNewState = CheckStartConditions(currentNode) ? false : !CheckStartConditions(rootNode);

        currentNode.action.OnUpdate();
        CheckExitConditions();

    }
    bool CheckStartConditions(BTNode node)
    {
        foreach (BTNode child in node.childNodes)
        {
            if (validStateFound) break;
            if (child.condition.StartCondition(this.gameObject))
                if (child.action != null)
                {
                    currentNode = child;
                    currentNode.action.OnSet(this.gameObject);
                    validStateFound = true;
                    return true;
                }
                else if (child.childNodes != null)
                    return CheckStartConditions(child);
                else
                {
                    Debug.Log($"Empty node {child.name} found");
                    return false;
                }

        }
        if (validStateFound)
            return true;
        else
            return false;
    }
    void CheckExitConditions()
    {
        if (currentNode.condition.ExitCondition())
        {
            currentNode.action.OnExit(this);
            if (requireNewState) validStateFound = false;
        }

    }
}
