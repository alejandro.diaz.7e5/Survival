using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public  class LevelUISceneCalls 
{
    static GameObject invPanel = GameObject.Find("Inventory");
    static GameObject msgPanel = GameObject.Find("PopMessage");

    public static void InitScript()
    {
        invPanel.SetActive(false);
        msgPanel.SetActive(false);
    }
    public static void OpenInventoryUI()
    {
        invPanel.SetActive(!invPanel.activeSelf);
        invPanel.GetComponent<InventoryManager>().LoadItems();
    }
    public static void ShowMessage(GameObject message)
    {
        msgPanel.SetActive(true);
        GameObject.Instantiate(message, msgPanel.transform);
    }
    public static void ClearMessage()
    {
        GameObject.Destroy(msgPanel.transform.GetChild(1).gameObject);
        msgPanel.SetActive(false);

    }
}
