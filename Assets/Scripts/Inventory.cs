using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory", menuName = "Scriptables/Inventory")]
public class Inventory : ScriptableObject
{
    public List<Item> items;
    public int[] itemCount;
}
