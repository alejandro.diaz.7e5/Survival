using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "Scriptables/Actions/Attack", fileName = "Attack action")]
public class AttackAction : ActionSO
{

    public NavMeshAgent agent;
    public Animator animator;
    private float attackCD;
    public float attackCDTime;
    public float damage;
    
    public override void OnSet(GameObject enemyBase)
    {
        agent = enemyBase.GetComponent<NavMeshAgent>();
        animator = enemyBase.GetComponent<Animator>();
        agent.isStopped = true;
    }
    public override void OnUpdate()
    {
        if (attackCD < 0)
        {
            animator.SetTrigger("Attack");
            attackCD = attackCDTime;
        }
        else 
            attackCD -= Time.deltaTime;
    }
    public override void OnExit(BTNodeManager manager)
    {
        manager.requireNewState = true;
        agent.isStopped = false;
    }
}
