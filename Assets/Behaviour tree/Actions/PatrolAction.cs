using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[CreateAssetMenu(menuName = "Scriptables/Actions/Patrol", fileName ="Patrol Action")]
class PatrolAction : ActionSO
{
    public Transform goal;
    public Vector3[] WalkPoints;
    public int[] route;
    public int routeIndex;
    public NavMeshAgent agent;
    public override void OnSet(GameObject enemyBase)
    {
        agent = enemyBase.GetComponent<NavMeshAgent>();
        agent.destination = WalkPoints[route[routeIndex]];
    }
    // Update is called once per frame
    public override void OnUpdate()
    {
        if (agent.remainingDistance < 0.5f)
        {
            routeIndex++;
            if (routeIndex >= route.Length) routeIndex = 0;
            if (route[routeIndex] == -1)
            {
                Stop();
                routeIndex++;
            }
            agent.SetDestination(WalkPoints[route[routeIndex]]);
        }
    }

    private void Stop()
    {
        agent.isStopped = true;
        float time = 5;
            while (time > 0)
                time-=Time.deltaTime;
        agent.isStopped = false;
    }

    public override void OnExit(BTNodeManager manager)
    {
        manager.requireNewState = true;
    }
}

