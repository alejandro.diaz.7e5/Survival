using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "Scriptables/Actions/Chase", fileName = "Chase Action")]
public class ChaseAction : ActionSO
{

    public NavMeshAgent agent;
    public override void OnSet(GameObject enemyBase)
    {
        agent = enemyBase.GetComponent<NavMeshAgent>();
        agent.destination = GameObject.FindGameObjectWithTag("Player").transform.position;
    }
    public override void OnUpdate()
    {
        if (agent.pathStatus == NavMeshPathStatus.PathComplete)
            agent.destination = GameObject.FindGameObjectWithTag("Player").transform.position;
            


    }

    private void Stop()
    {
        agent.isStopped = true;
        float time = 5;
        while (time > 0)
            time -= Time.deltaTime;
        agent.isStopped = false;
    }

    public override void OnExit(BTNodeManager manager)
    {
        manager.requireNewState = true;
    }
}
