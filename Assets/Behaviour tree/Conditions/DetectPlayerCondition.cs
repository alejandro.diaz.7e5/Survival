
using UnityEngine;
[CreateAssetMenu(menuName = "Scriptables/Conditions/DetectPlayer", fileName = "Detect Player")]
public class DetectPlayerCondition : ConditionSO
{
    public GameObject enemy;
    public float detectMaxDistance;
    public bool mustDetect;

    public override bool ExitCondition()
    {
        return !StartCondition(enemy);
    }

    public override bool StartCondition(GameObject caller)
    {
        enemy = caller;
        GameObject player = GameObject.FindWithTag("Player");
        if (Vector3.Distance(player.transform.position, enemy.transform.position) < detectMaxDistance)
            return mustDetect ? true : false;
        else
            return mustDetect ? false: true;

    }
}
