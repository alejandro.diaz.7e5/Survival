using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Scriptables/Conditions/Detect Player In Range", fileName = "Detect Player Range")]
public class DetectPlayerRange : DetectPlayerCondition
{
    public float detectMinDistance;
    // Start is called before the first frame update


    // Update is called once per frame
    public override bool StartCondition(GameObject caller)
    {
        enemy = caller;
        GameObject player = GameObject.FindWithTag("Player");
        float distance = Vector3.Distance(player.transform.position, enemy.transform.position);
        if (distance > detectMinDistance && distance < detectMaxDistance)
            return mustDetect ? true : false;
        else
            return mustDetect ? false : true;
    }
}
