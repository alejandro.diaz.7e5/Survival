using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Scriptables/Conditions/Empty", fileName = "Empty Condition")]
public class EmptyCondition : ConditionSO
{
    public override bool ExitCondition()
    {
        return true;
    }

    public override bool StartCondition(GameObject caller)
    {
        return true;
    }
}
