﻿# Welcome to the Survival

## **Instructions**

**WASD/Arrow Keys** to **move**
**Space** to **Jump**
**C** to **Crouch**
**LShift** to **Sprint**
**E** to **Aim**
**Left Click** to **Shoot**
**B** to **Celebrate**
**Q** to **Pick  up an item**


